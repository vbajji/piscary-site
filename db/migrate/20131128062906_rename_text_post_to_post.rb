class RenameTextPostToPost < ActiveRecord::Migration
  def change
    rename_table :text_posts, :posts
  end
end
