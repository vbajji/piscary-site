# Piscary

This is the site source tree for Piscary Club at West High School in
Salt Lake City, UT.

This site is a blog written in [Ruby on Rails][rails].

## What is "Piscary"

"Piscary" (prounounced piss-kuh-ree) is a fancy word for
fishing. Piscary Club is a general outdoor and recreation club. We try
to integrate the outdoors into various fields of study, including, but
not limited to, photography, programming, and biology.

# Contributing

Anyone is welcome to contribute and/or fork the project.

We use [git flow][gf] for our branch management. It may appear chaotic, but
there is a method to the madness.

## Issues

Everyone knows, the best method for writing an essay is to simply put pen to
paper, and then go back and refine your work. We take the same philosophy with
coding.

It is always best to, initially, write code that works, regardless of
conventions or best practices. Then, go back, and fix it later.

For now, we are just trying to get a working version of the site. We will fix
any issues after we have a working copy.

We use BitBucket's issue-tracking system to, well, keep track of these issues we
write into the site. This way, we have an ordered and prioritized list when we
go back to fix them later. Furthermore, anyone who wants to can fix the issues
for us.

# Various Copyrights

Piscary Club is Copyright (c) 2013 Harrison Unruh and Peter
Harpending.

Information about copying, distributing, modifying, et cetera, can be found in
the LICENSE file.

[rails]: http://rubyonrails.org/
[gf]: https://github.com/nvie/gitflow
