**HTML Standards for Piscary Project**

Version 0.2, 17 November 2013

This document outlines the coding standards for HTML code in the
Piscary Website project.

# Cleanliness

## Line length

You must follow the 80-character rule. That means, no lines longer
than 80 characters. If you have an egregiously long line, wrap it up
like this.

```html
<!-- no -->
<some-obscene-tag parameter="some obscenely long string." another-parameter="some other string.">

<!-- ideal-->
<some-obscene-tag parameter="some obscenely long string."
                  another-parameter="some other string.">

<!-- also acceptable; use more than one indent for parameters -->
<some-obscene-tag
    parameter="some obscenely long string."
    another-parameter="some other string.">

<!-- This is not acceptable, however, because the parameters could -->
<!-- easily be confused with the text inside of a subsequent block.-->
<some-obscene-tag
  parameter="some obscenely long string."
  another-parameter="some other string.">
```

## Indents

Indents are two spaces. Yes, I know this conflicts with four-space
indents in PHP. If you have a half-decent editor (i.e. **not**
Substandard Text 2), then you should be fine.

## Comments

There is only one comment syntax in HTML. Use it on a line-per-line
basis, rather than block-commenting a region. This makes it much
easier to *un*comment said region.

```html
<head>
  <title>Some dumb title</title>
  <link rel="I dont care" href="narstarstart">

  <meta text="This is some text. This is not actual HTML code.">
  <meta text="um... um... the... um... meta tags">
</head>
```

Properly commented: 
```html
<!-- <head> -->
<!--   <title>Some dumb title</title> -->
<!--   <link rel="I dont care" href="narstarstart"> -->

<!--   <meta text="This is some text. This is not actual HTML code."> -->
<!--   <meta text="um... um... the... um... meta tags"> -->
<!-- </head> -->
```

Improperly commented:
```html
<!--<head>
  <title>Some dumb title</title>
  <link rel="I dont care" href="narstarstart">

  <meta text="This is some text. This is not actual HTML code.">
  <meta text="um... um... the... um... meta tags">
</head>-->
```
