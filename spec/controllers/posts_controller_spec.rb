require 'spec_helper'

describe PostsController do

  describe "GET 'modify'" do
    it "returns http success" do
      get 'modify'
      response.should be_success
    end
  end

  describe "The show controller" do
    before do
      params = {
        title: "How I Met Your Mother",
        text: "Paternity *suit*."
      }
      @my_post = Post.create(params)

      @orig_text = @my_post.text 
      @new_text = `echo "#{@orig_text}" | redcarpet -`

      get 'show', id: @my_post.id
    end

    it "Should convert the text from Markdown into HTML." do
      assigns(:text_html).should_not be_nil
      assigns(:text_html).should eql @new_text
    end
  end # The show controller
end
