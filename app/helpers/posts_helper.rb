require 'redcarpet'

module PostsHelper
  def render_html(md_text)
    markdown_opts = {
      autolink: true,             # Automatically link URLs
      disable_indented_code_blocks: true, # Disable traditional Markdown code blocks.
      fenced_code_blocks: true,   # Use GitHub-style ```...``` tags
      footnotes: true,            # Footnotes
      highlight: true,            # Allow HTML highlighting
      lax_spacing: true,          # Be more lenient with inline html
      quote: true,                # Parse quotations
      strikethrough: true,        # Allow ~~something~~ for strikethrough
      superscript: true,          # Allow superscripts
      tables: true,               # Allow tables
      underline: true,            # Allow underlines
    }
    converter = Redcarpet::Markdown.new(Redcarpet::Render::HTML, markdown_opts)
    converter.render(md_text)
  end
end
