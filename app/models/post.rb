class Post < ActiveRecord::Base
  attr_accessible :title, :text

  # Title-related
  validates :title,
    presence: true,
    length: { minimum: 1,
              maximum: 150 }

  # Text-related
  validates :text,
    presence: true
end # Post
