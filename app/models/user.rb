class User < ActiveRecord::Base
  attr_accessible :username, :password, :password_confirmation, :email

  # Username validation
  validates :username,
    presence: true,
    uniqueness: { case_sensitive: false },
    length: { maximum: 50,
              minimum: 3 }

  # Password validation
  validates :password,
    presence: true,
    length: { minimum: 8 }  
  validates :password_confirmation,
    presence: true
  has_secure_password

  # Email validation
  # Email is optional, ftr
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email,
    format: { with: VALID_EMAIL_REGEX },
    unless: "email.nil? or email.strip.empty?"

  has_many :posts
end # User
