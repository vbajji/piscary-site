require 'redcarpet'
require 'posts_helper'
include PostsHelper

class PostsController < ApplicationController
  def create
  end

  def modify
  end

  def show
    @post = Post.find(params[:id])
    @text_html = render_html(@post.text)
    puts @text_html
  end
end
