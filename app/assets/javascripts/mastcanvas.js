(function() {
    var canvas = document.getElementById('mastcanvas'),
        context = canvas.getContext('2d');

    // resize the canvas to fill browser window dynamically
    window.addEventListener('resize', resizeCanvas, false);

    function resizeCanvas() {
	canvas.width = $(window).width();
	canvas.height = 60;

	/**
	 * Your drawings need to be inside this function otherwise they will be reset when 
	 * you resize the browser window and the canvas goes will be cleared.
	 */
	drawStuff(); 
    }
    resizeCanvas();

    function drawStuff() {
        // Create gradient
        var grd = context.createLinearGradient(0,0,0,60);
        grd.addColorStop(0,"#444444");
        grd.addColorStop(1,"#333333");

        // Fill with gradient
        var vnw = $(window).width();
        context.fillStyle = grd;
        context.fillRect(0, 0, vnw, 60);
    }//end of drawStuff()
})();
