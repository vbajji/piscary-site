(function() {
    window.addEventListener('resize', splitJumboTron, false);

    function splitJumboTron() {
        var vpw = $(window).width() - 20;
        $(".jumbotron-container").css("width", "" + vpw + "px");
        $(".jumbotron-container").css("marginLeft", "10px");
        $(".jumbotron-container").css("marginRight", "10px");

        $(".jumbonav").remove();

        var nav_columns = Math.floor((vpw - 700) / 400)
        for(var i=0; i<nav_columns; i++) {
            $(".jumbotron-row").append("<td class=\"jumbonav\"></td>")
        }
    }
    splitJumboTron();
})();
