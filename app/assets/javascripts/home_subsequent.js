(function() {
    window.addEventListener('resize', resizeSubsequentBoxes, false);
    
    function resizeSubsequentBoxes() {
        var vpw = $(window).width() - 20;
        $(".subsequent").css("width", "" + vpw + "px");
        $(".subsequent").css("marginLeft", "10px");
        $(".subsequent").css("marginRight", "10px");
    }//end of resizeSubsequentBoxes()
    resizeSubsequentBoxes();
})();
